FROM tiangolo/uwsgi-nginx-flask

MAINTAINER Ibrahim Menem <ibrahim.menem@bics.com>

RUN pip install Jinja2

RUN  mkdir /etc/nginx/ssl
RUN  openssl req -batch -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl/nginx.key -out /etc/nginx/ssl/nginx.crt

COPY WWW static
COPY docker/nginx.conf /etc/nginx/conf.d/nginx.conf
COPY docker/main.py main.py



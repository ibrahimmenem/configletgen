#!/usr/bin/env python
from flask import Flask
from flask import request
from flask import jsonify
from jinja2 import Template

app = Flask(__name__)

NOKIA_EPL_TEMPLATE = Template("""
service vpls {{ GLOBAL.vcid }} customer 1 create
    service-name "vpls-testcust-{{GLOBAL.vcid }}"
    fdb-table-size 100
    stp
        shutdown
    exit
    sap {{ HOST.port }}:{{ HOST.outer_vlan }}.{{ HOST.inner_vlan }} create
        description "some description"
        shutdown
    exit

    no shutdown
exit
""")


@app.route("/api/getconfig", methods=['POST'])
def get_config():
    epl_abstract_data = request.get_json(force=True, silent=False)
    response = {}
    response["nodename"] = epl_abstract_data["nodename"]
    selected_pe = None
    try:
        selected_pe = epl_abstract_data.get("hosts")[str(epl_abstract_data.get("nodename"))]
    except KeyError:
        return
    response["template"] = NOKIA_EPL_TEMPLATE.render(
        GLOBAL=epl_abstract_data.get("globalCfg"),
        HOST=selected_pe)
    return jsonify(response)

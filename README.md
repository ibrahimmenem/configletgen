## General
Create Static configlet from web GUI, input variables into text boxes 
and the Configlet will be generated automatically for you 

## Demo
![alt text][Demo]

[Demo]: demo/configletgen.mp4 "Demo"

## Build
```
./rebuildall
```    

## Requirements

* Python
* Jinja2
* Nginx
* Flask

Or

* Docker 
 
$(function() {
           const get_config_api_url = 'api/getconfig';

           var dialog, form, hosts = {},
               globalCfg = {},
               postData = {},

               bandwidth = $("#bandwidth"),
               epl_name = $("#epl_name"),
               codif = $("#codif"),
               vcid = $("#vcid"),

               hostname = $("#hostname"),
               abrekey = $("#abrekey"),
               inner_vlan = $("#inner_vlan"),
               outer_vlan = $("#outer_vlan"),
               port = $("#port");

           function saveGlobalConfig() {
               globalCfg["bandwidth"] = bandwidth[0].value;
               globalCfg["epl_name"] = epl_name[0].value;
               globalCfg["codif"] = codif[0].value;
               globalCfg["vcid"] = vcid[0].value;
           }
           function saveNodeConfig() {

               hosts[dialog.activeNode] = {};
               hosts[dialog.activeNode]["hostname"] = hostname[0].value;
               hosts[dialog.activeNode]["abrekey"] = abrekey[0].value;
               hosts[dialog.activeNode]["inner_vlan"] = inner_vlan[0].value;
               hosts[dialog.activeNode]["outer_vlan"] = outer_vlan[0].value;
               hosts[dialog.activeNode]["port"] = port[0].value;

               dialog.dialog("close");
               return true;
           }

           // config dialog
           dialog = $("#dialog-form").dialog({
               autoOpen: false,
               height: 500,
               width: 500,
               modal: true,
               buttons: {
                   "Save config": saveNodeConfig,
                   Cancel: function() {
                       dialog.dialog("close");
                   }
               },
               close: function() {
                   form[0].reset();
               }
           });

           form = dialog.find("form").on("submit", function(event) {
               event.preventDefault();
               saveNodeConfig();
           });

           pe1 = document.getElementById('pe1');
           pe1.onclick = function(e) {
               dialog.activeNode = 'pe1',
               e.preventDefault();
               dialog.dialog("open");
           };

           pe2 = document.getElementById('pe2');
           pe2.onclick = function(e) {
               dialog.activeNode = 'pe2',
               e.preventDefault();
               dialog.dialog("open");
           };


           // config display tabs
           $("#tabs").tabs({

               activate: function(event, ui) {

                   // prepare data to send to server
                   saveGlobalConfig();
                   postData = {
                       nodename: ui.newTab[0].innerText,
                       globalCfg: globalCfg,
                       hosts: hosts
                   };

                   // this is a hack, use tab integrated ajax instead
                   $.ajax({
                       type: 'POST',
                       data: JSON.stringify(postData),
                       url: get_config_api_url ,
                       error: function(xhr, status, index, anchor) {
                           console.log("Couldn't load this tab. We'll try to fix this as soon as possible. ");
                       },
                       success: function(xhr, status, index, anchor) {
                           $('#tabs-' + xhr.nodename).text(xhr.template);
                       }
                   });

               }

           });
 } );